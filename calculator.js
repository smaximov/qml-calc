var lastType = null;
var lastValue = null;
var lastOperator = null;
var accum = null;

var operatorTable = {
    "+": function(a, b) { return a + b; },
    "−": function(a, b) { return a - b; },
    "×": function(a, b) { return a * b; },
    "÷": function(a, b) { return a / b; },
};

function negate() {
    if (display.getText().indexOf("-") >= 0) {
        return;
    }

    display.prependText("-");
}

function applyOperator() {
    if (lastOperator === null) {
        return;
    }

    lastValue = lastValue || display.getText();

    var operator = operatorTable[lastOperator];
    var op1 = parseFloat(accum);
    var op2 = parseFloat(lastValue);
    var value = operator(op1, op2);
    accum = value.toString();

    display.setText(value);
}

function clear() {
    lastType = lastValue = lastOperator = accum = null;
    display.clear();
}

function dropChar() {
    if (lastType === ButtonType.Operator || lastType === ButtonType.Eval) {
        return;
    }

    display.dropChar();
}

function pushOperator(opName) {
    if (lastType === null) {
        if (opName === "−") {
            negate();
        }
        return;
    }

    if (lastType !== ButtonType.Eval) {
        if (lastOperator) {

            applyOperator();
        } else {
            accum = display.getText();
        }
    }

    lastOperator = opName;
}

function pushDigit(digit) {
    lastValue = null;

    var text = display.getText();

    if ((/^-?0$/).test(text)) {
        display.setText(text.replace("0", digit));
        return;
    }

    if (lastType === ButtonType.Operator || lastType === ButtonType.Eval) {
        display.setText(digit);
        return;
    }

    display.appendText(digit);
}

function pushDot() {
    if ((/\./).test(display.getText())) {
        return;
    }

    display.appendText(".");
}
