import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Dialogs 1.1
import QtQuick.Layouts 1.1

import calc.types.button 1.0

import "widgets"
import "calculator.js" as Calculator

ApplicationWindow {
    visible: true
    title: qsTr("Калькулятор")
    id: window
    width: 250
    height: 300

    readonly property int margins: 10

    menuBar: MenuBar {
        Menu {
            title: qsTr("Файл")
            MenuItem {
                text: qsTr("О программе")
                onTriggered: aboutDialog.open()
            }
            MenuSeparator {}
            MenuItem {
                text: qsTr("Выход")
                onTriggered: Qt.quit();
            }
        }
    }

    MessageDialog {
        id: aboutDialog
        title: qsTr("О программе")
        text: "Commit Masters Studio © 2014"
        informativeText: qsTr("Пример калькулятора")
        icon: StandardIcon.Information
    }

    property alias display: numpad.display

    NumberPad {
        id: numpad

        onButtonClicked: {
            switch (type) {
            case ButtonType.Digit:
                Calculator.pushDigit(text);
                break;

            case ButtonType.Operator:
                Calculator.pushOperator(text);
                break;

            case ButtonType.Dot:
                Calculator.pushDot();
                break;

            case ButtonType.Eval:
                Calculator.applyOperator();
                break;

            case ButtonType.Clear:
                Calculator.clear();
                break;

            case ButtonType.Delete:
                Calculator.dropChar();
                break;
            }

            if ([ButtonType.Digit,
                 ButtonType.Dot,
                 ButtonType.Operator,
                 ButtonType.Eval].indexOf(type) >= 0) {
                Calculator.lastType = type;
            }
        }
    }
}
