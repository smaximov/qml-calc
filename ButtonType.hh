#ifndef BUTTONTYPE_HH
#define BUTTONTYPE_HH

#include <QObject>
#include <QtQml>

class ButtonTypeWrapper : public QObject
{
    Q_OBJECT
    Q_ENUMS(ButtonType)
public:
    enum ButtonType
    {
        Digit,
        Operator,
        Dot,
        Eval,
        Clear,
        Delete
    };

    static void registerType()
    {
        qmlRegisterUncreatableType<ButtonTypeWrapper>(
                    "calc.types.button",          // Library uri
                    1, 0,                         // Major, Minor
                    "ButtonType",                 // Type name
                    "Unable to load ButtonType"); // Failure message
    }
};

#endif // BUTTONTYPE_HH
