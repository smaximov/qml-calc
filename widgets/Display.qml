import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1

Rectangle {
    property alias text: textItem.text

    Layout.fillWidth: true
    Layout.minimumHeight: window.height / 8
    Layout.minimumWidth: 300

    Text {
        id: textItem
        text: "0"
        horizontalAlignment: Text.AlignRight
        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            rightMargin: 5
        }
        font.pointSize: 20
    }

    function appendText(val) {
        text += val.toString();
    }

    function setText(val) {
        if (val === Number.POSITIVE_INFINITY) {
            text = "∞";
            return;
        }

        if (val === Number.NEGATIVE_INFINITY) {
            text = "-∞";
            return;
        }

        text = val.toString();
    }

    function getText() {
        return text;
    }

    function clear() {
        text = "0";
    }

    function prependText(val) {
        text = val.toString() + text;
    }

    function dropChar() {
        if (/^-?\d$/.test(text)) {
            text = "0";
            return;
        }

        text = text.substr(0, text.length - 1);
    }
}
