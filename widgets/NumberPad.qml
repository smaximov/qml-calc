import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1

import calc.types.button 1.0

GridLayout {
    columns: 4
    columnSpacing: 7
    rowSpacing: 7

    property alias display: displayWidget

    anchors {
        fill: parent
        margins: window.margins
    }

    signal buttonClicked(int type, string text)

    Display {
        id: displayWidget
        Layout.columnSpan: 4
    }

    Item { Layout.fillHeight: true; Layout.fillWidth: true; }
    PadButton { text: "Очистить"; Layout.columnSpan: 2; type: ButtonType.Clear }
    PadButton { text: "←"; type: ButtonType.Delete }
    PadButton { text: "7" }
    PadButton { text: "8" }
    PadButton { text: "9" }
    PadButton { text: "+"; type: ButtonType.Operator }
    PadButton { text: "4" }
    PadButton { text: "5" }
    PadButton { text: "6" }
    PadButton { text: "−"; type: ButtonType.Operator }
    PadButton { text: "1" }
    PadButton { text: "2" }
    PadButton { text: "3" }
    PadButton { text: "×"; type: ButtonType.Operator }
    PadButton { text: "."; type: ButtonType.Dot }
    PadButton { text: "0" }
    PadButton { text: "="; type: ButtonType.Eval }
    PadButton { text: "÷"; type: ButtonType.Operator }
}
