import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0

import calc.types.button 1.0

Item {
    Layout.fillHeight: true
    Layout.fillWidth: true
    Layout.minimumHeight: 30
    Layout.minimumWidth: 30

    id: root
    state: "normal"
    states: [
        State {
            name: "normal"
            PropertyChanges {
                target: dropShadow
                visible: true
            }
            PropertyChanges {
                target: innerShadow
                visible: false
            }
        },
        State {
            name: "pressed"
            PropertyChanges {
                target: dropShadow
                visible: false
            }
            PropertyChanges {
                target: innerShadow
                visible: true
            }
        }

    ]

    property int type: ButtonType.Digit
    property alias text: textItem.text
    property alias color: rect.color

    readonly property string shadowColor: "#80000000"
    readonly property int shadowSamples: 64

    DropShadow {
        id: dropShadow
        samples: root.shadowSamples
        color: root.shadowColor
        horizontalOffset: 2
        verticalOffset: 2
        source: rect
        anchors.fill: rect
    }    

    Timer {
        id: timer
        interval: 100
        onTriggered: {
            root.state = "normal"
        }
    }

    Rectangle {
        anchors.fill: root
        id: rect

        Text {
            id: textItem
            anchors.centerIn: parent
            font.pointSize: 18
        }

        MouseArea {
            anchors {
                fill: parent
                margins: -5
            }

            onClicked: {
                root.state = "pressed"
                timer.start();
                root.parent.buttonClicked(type, text);
            }
        }
    }

    InnerShadow {
        id: innerShadow
        samples: root.shadowSamples
        color: root.shadowColor
        horizontalOffset: 2
        verticalOffset: 2
        source: rect
        anchors.fill: rect
    }
}
