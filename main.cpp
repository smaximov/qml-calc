#include <QApplication>
#include <QQmlApplicationEngine>


#include "ButtonType.hh"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    ButtonTypeWrapper::registerType();

    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}
